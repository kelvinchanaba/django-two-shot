from django import forms

# Create your models here.


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput
    )


# include widget PasswordInput for passworld field


# name and write a function
# this function will display form "when" GET method (if statements with get)

# try to log te person in when it is a post method (if statements with post)
